A lightweight planar range simulator in MATLAB.

--------------------------------------------------
Examples:
examples/mapCreation.m: Walks through creation of a map and use of the simulator.
examples/corridorSimulation.m: Range simulation in a corridor.

--------------------------------------------------
Details of simulator:
The laser has 360 equally spaced bearings. 
The null reading is 0.
Length units are m. The max reading is 4.5m.
Maps are as collections of straight lines.

This is a fast version of a more elaborate planar range simulator. For
more details, refer to: 
Construction and Validation of a High-Fidelity
Simulator for a Planar Range Sensor, Abhijeet Tallavajhula and Alonzo
Kelly, ICRA 2015

--------------------------------------------------
Authors: 
Abhijeet Tallavajhula, atallav1@andrew.cmu.edu
Michael Shomin, mshomin@andrew.cmu.edu

--------------------------------------------------
This material is based upon work supported by the National Science
Foundation under Grant Number 1317803. Any opinions, findings, and
conclusions or recommendations expressed in this material are those of
the author(s) and do not necessarily reflect the views of the National
Science Foundation.


