% Load corridor map.
load corridor
map.plot();

%% Simulate ranges in corridor.
load laser_simulator
lsim.setMap(map);
pose = [1.5; 5; deg2rad(120)];
ranges = lsim.simulate(pose);

%% Plot ranges.
bearings = deg2rad(0:359);
x = pose(1)+ranges.*cos(bearings+pose(3));
y = pose(2)+ranges.*sin(bearings+pose(3));
figure; plot(x,y,'r.');
xlabel('x (m)'); ylabel('y (m)'); axis equal;