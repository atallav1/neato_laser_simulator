% Create a lineMap from lineObjects.

% Create walls.
walls = lineObject();
walls.lines = [0 5; 0 0; 5 0];

% Create obstacles
obs1 = lineObject();
obs1.lines = [-0.25 -0.25; 0.25 -0.25; 0.25 0.25; -0.25 0.25; -0.25 -0.25];
obs1.pose = [3; 3; 0];

obs2 = lineObject;
obs2.lines = [1 0; 0 0.4; -0.4 0; 0 -0.8; 1 0];
obs2.pose = [1; 2; deg2rad(20)];

map = lineMap([walls obs1 obs2]);
map.plot();

%% Load laser simulator.
load laser_simulator
lsim.setMap(map);

%% Simulate ranges
pose = [3; 1; deg2rad(90)];
ranges = lsim.simulate(pose);

%% Plot ranges
bearings = deg2rad(0:359);
x = pose(1)+ranges.*cos(bearings+pose(3));
y = pose(2)+ranges.*sin(bearings+pose(3));
figure; plot(x,y,'r.');
xlabel('x (m)'); ylabel('y (m)'); axis equal;

