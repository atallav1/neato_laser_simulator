classdef lineMap < handle
	%lineMap Map constructed of lineObjects.
	% Authors: Abhijeet Tallavajhula, Michael Shomin
	
	properties (Constant = true)
		sampleResln = 0.05; 
	end
	
	properties (GetAccess = public, SetAccess = private)
		num_objects = 0;
		objects;
	end
	
	methods (Access = public)
		function lm = lineMap(objects)
			if( nargin < 1)
				error('You need to intialize lineMap with at least one object');
			end
			
			for obj = objects
				lm.addObject(obj);
			end
		end
		
		function n = addObject(lm, object)
			%ADDOBJECT Add to map
			%
			% n = ADDOBJECT(lm, object)
			%
			% object  - lineObject instance.
			%
			% n       - Total number of objects in map.
			
			if( isempty(object.lines))
				error('Object must have line of size n x 2 (n>0)');
			else
				if ~isempty(object.path)
					object.has_path = true;
					object.pose = object.path(1,1:3)';
				else
					object.has_path = false;
				end
				object.startTimer();
				
				object.update();
				
				if isempty(lm.objects)
					lm.objects = object;
				else
					lm.objects(end + 1) = object;
				end
				lm.num_objects = lm.num_objects + 1;
				lm.objects(end).id = lm.num_objects;
				n = lm.num_objects;
			end
		end
		
		function hfig = plot(obj)
			hfig = figure; hold on;
			for object = obj.objects
				object.update();
				object.plot();
			end
			axis equal
			hold off
		end
		
		function [ranges,incidence_angles] = getRAlpha(obj,pose,max_range,ang_range)
			%GETRALPHA Get ranges and angles of incidence. Out-of-range
			% values are NaN.
			%
			% [ranges,incidence_angles] = GETRALPHA(obj,pose,max_range,ang_range)
			%
			% pose             - Array of size 3.
			% max_range        - Maximum range in m.
			% ang_range        - Array of angles in rad.
			%
			% ranges           - Ranges in m.
			% incidence_angles - Incidence angles in rad.
			
			sweep = pose(3) + ang_range;
			
			p0x = pose(1);
			p0y = pose(2);
			
			p1x = p0x + max_range.*cos(sweep);
			p1y = p0y + max_range.*sin(sweep);
			
			s1x = p1x - p0x;
			s1y = p1y - p0y;
			
			lc = cell2mat({obj.objects(:).line_coords}');
			
			% start points of lines
			p2x = lc(1:end-1,1);
			p2y = lc(1:end-1,2);
			
			% end points of lines
			p3x = lc(2:end,1);
			p3y = lc(2:end,2);
			
			% line segments
			s2x = p3x - p2x;
			s2y = p3y - p2y;
			
			% s takes into account that the line segment is finite
			s = ((p2x-p0x)*s1y + (p0y-p2y)*s1x)./...
				(-s2x*s1y + s2y*s1x);
			
			% t takes into account that the rays are finite
			t = repmat(( s2x.*(p0y-p2y) - s2y.*(p0x-p2x)),1,length(s1x))...
				./ (-s2x*s1y + s2y*s1x);
			
			col = s >= 0 & s <= 1 & t >= 0 & t <= 1;
			t(~isfinite(t)) = 0;
			cpx = (t .* repmat(s1x,length(s2x),1)) .* col;
			cpy = (t .* repmat(s1y,length(s2x),1)) .* col;
			r = (cpx.^2 + cpy.^2).^.5;
			
			r(~col) = nan;
			
			% angles of incidence to each line segment
			% currently has positive/ negative ambiguity
			alpha = zeros(length(p2x),1);
			for i = 1:length(p2x)
				params = ParametrizePts2ABC(lc(i,1:2),lc(i+1,1:2));
				alpha(i) = atanLine2D(params(1),params(2));
			end
			incidence_angles = repmat(alpha,1,length(ang_range))-repmat(mod(ang_range+pose(3),pi),length(alpha),1);
			incidence_angles(incidence_angles == 0) = pi;
			incidence_angles = incidence_angles-pi/2*ones(size(incidence_angles)).*sign(incidence_angles);
			
			% remove spurious line segments between points not belonging to same
			% object
			ng = cumsum(cellfun(@(x) size(x,1), {obj.objects(:).line_coords}'));
			r(ng(1:end-1),:) = [];
			incidence_angles(ng(1:end-1),:) = [];
			
			% get minimum range and angle to that segment
			[ranges, min_sub] = min(r,[],1);
			ids = sub2ind(size(incidence_angles),min_sub,1:length(ang_range));
			incidence_angles = incidence_angles(ids);
			incidence_angles(isnan(ranges)) = nan;
			% return absolute value of incidence angles
			incidence_angles = abs(incidence_angles);
		end
		
		function [ranges,incidence_angles] = raycast(obj, pose, max_range, ang_range)
			%RAYCAST Get ranges and angles of incidence. Out-of-range
			% values are 0.
			
			[ranges,incidence_angles] = obj.getRAlpha(pose,max_range,ang_range);
			ranges(isnan(ranges)) = 0;
		end
		
		function [ranges,angles] = raycastNoisy(lm, pose, max_range, ang_range)
			%RAYCASTNOISY Raycast with aritifical noise.
			%
			% [ranges,angles] = RAYCASTNOISY(lm, pose, max_range, ang_range)
			[ranges,angles] = raycast(lm, pose, max_range, ang_range);
			
			K = 1e-3;
			biasFactor = 0.05;
			for i = 1:length(ranges);
				sigma = K*ranges(i)^2/cos(angles(i));
				if isnan(sigma)
					continue;
				end
				ranges(i) = ranges(i)+ranges(i)*biasFactor+sigma*randn;
			end
		end
		
		function removeObject(lm, i)
			%REMOVEOBJECT
			%
			% REMOVEOBJECT(lm, i)
			%
			% i - Object index.
			
			lm.objects(i).lines = [];
			delete(lm.objects(i).h)
			lm.num_objects = lm.num_objects - 1;
		end
		
		function update(lm)
			%UPDATE Update map state.
			%
			% UPDATE(lm)
			for i = 1:length(lm.objects)
				lm.objects(i).update();
				lm.objects(i).plot();
			end
		end
	end
	
	methods (Static = true)
		function pts = sampleFromLines(lineArray,resln)
			%SAMPLEFROMLINES
			%
			% pts = SAMPLEFROMLINES(lineArray,resln)
			%
			% lineArray - Array of lineObject instances.
			% resln     - In m.
			%
			% pts       - Samples points.
			
			pts = [];
			nLines = length(lineArray);
			for i = 1:nLines
				lines = lineArray(i).lines;
				for j = 1:size(lines,1)-1;
					p1 = lines(j,:);
					p2 = lines(j+1,:);
					p12 = p2-p1;
					temp = [0:resln/norm(p12):1]'*p12;
					temp = bsxfun(@plus,temp,p1);
					pts = [pts; temp];
				end
			end
			
		end
	end
end
